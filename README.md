# Building a RESTful API with Rails
This API is built with Ruby on Rails and is giving 3 endpoints to work with: 
1. `GET /api/getcreditscore` - returns random credit score
2. `GET /api/getmincreditscore` - returns min credit score available
3. `GET /api/getmaxcreditscore` - returns max credit score available
## Getting Started
Follow these steps to set up and run the API:
1. Clone this repository to your local machine.
2. In the `.env` file put all your environment variables from PostgreSQL.
3. Run following commands to build and run the API:
    
```bash
docker-compose build
docker-compose up -d
```
```
4. Open Postman and copy the `ISV3-98.postman_collection.json` file into it to verify the endpoints.