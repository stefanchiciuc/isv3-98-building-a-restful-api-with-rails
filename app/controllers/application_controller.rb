# frozen_string_literal: true

class ApplicationController < ActionController::API
  def creditscore
    render json: { creditscore: rand(200..900) }
  end

  def mincreditscore
    render json: { creditscore: 200 }
  end

  def maxcreditscore
    render json: { creditscore: 900 }
  end
end
