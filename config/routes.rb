# frozen_string_literal: true

Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get 'api/getcreditscore', to: 'application#creditscore'
  get 'api/getmincreditscore', to: 'application#mincreditscore'
  get 'api/getmaxcreditscore', to: 'application#maxcreditscore'
end
